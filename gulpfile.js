'use strict';

var gulp = require('gulp'),
		watch = require('gulp-watch'),
		prefixer = require('gulp-autoprefixer'),
		sass = require('gulp-sass'),
		rename = require('gulp-rename'),
		plumber = require('gulp-plumber'),
		notify = require('gulp-notify');


var path = {
	src: {
		style: ['app/css/**/*.scss', '!app/css/**/_*.scss']
	}
}


gulp.task('sass', function(){
	gulp.src(path.src.style)
			.pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
			.pipe(sass({outputStyle: 'compressed'}))
			.pipe(prefixer())
			//.pipe(rename({ suffix: '.min' }))
			.pipe(gulp.dest(function(file){
    		return file.base;
    	}))
});


gulp.task('sass:watch', function () {
  gulp.watch(path.src.style, ['sass']);
});


gulp.task('default', ['sass:watch']);