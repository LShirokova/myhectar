<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
		<title>Document</title>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jScrollPane/2.1.2/style/jquery.jscrollpane.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.min.css">

		<link rel="stylesheet" href="web/fonts/fonts.css">
		<link rel="stylesheet" href="app/css/app.css">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	</head>
	<body>
		<?php include_once 'views/_header.php'; ?>
		<?php include 'views/_top.php'; ?>
		<?php include_once 'views/_survey.php'; ?>
		<?php include_once 'views/_land-selection.php'; ?>
		<?php include_once 'views/_advantages.php'; ?>
		<?php include_once 'views/_reasons.php'; ?>
		<?php include_once 'views/_ways.php'; ?>
		<?php include_once 'views/_about.php'; ?>

		<?php include_once 'views/_purchase-step.php'; ?>
		<?php include_once 'views/_comparison-faq.php'; ?>
		<?php include_once 'views/_more-questions.php'; ?>
		<?php include_once 'views/_booking.php'; ?>
		<?php include_once 'views/_map.php'; ?>
		<?php include_once 'views/_credit.php'; ?>
		<?php include 'views/_top.php'; ?>
		<?php include_once 'views/_footer.php'; ?>

		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jScrollPane/2.1.2/script/jquery.jscrollpane.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
		<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js" type="text/javascript"></script>
		<script src="web/js/common.js"></script>
	</body>
</html>