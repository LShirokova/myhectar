<link rel="stylesheet" href="app/css/w/ways/2.0.0.css">
<section class="w-ways__wrapper">
	<div class="w-ways">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<h2 class="h2 text-center"><b>Как еще можно использовать</b> большой участок?</h2>
				</div><!--col-->
			</div><!--.row-->
		</div><!--.container-fluid-->
	</div><!--.w-ways-->
	<div class="w-ways w-ways_2">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6">
					<div class="w-ways__container">
						<div class="w-ways__container-t">
							<h3 class="h3">Под мини-ферму</h3>
							<ul class="w-ul">
								<li class="w-li">Каким делом заняться на&nbsp;своей ферме?</li>
								<li class="w-li">Почему спрос на&nbsp;фермерские продукты растет?</li>
								<li class="w-li">Как найти низкоконкурентный рынок сбыта?</li>
								<li class="w-li">Какие госпрограммы компенсируют ваши расходы?</li>
								<li class="w-li">Как стабилизировать прибыль на&nbsp;100&nbsp;тыс. руб./мес.?</li>
								<li class="w-li">Кейсы, кейсы, кейсы!</li>
							</ul>
						</div><!--.w-ways__container-t-->
						<div class="w-ways__container-b">
							<p>
								<a href="#" class="btn button_green button_green_sm">Получить материалы</a>
							</p>
						</div><!--.w-ways__container-b-->
					</div><!--.w-ways__container-->
				</div><!--col-->
				<div class="col-md-6">
					<div class="w-ways__container">
						<div class="w-ways__container-t">
							<h3 class="h3">Как выгодную инвестицию</h3>
							<ul class="w-ul">
								<li class="w-li">Почему вложения в&nbsp;землю лидируют по&nbsp;надежности?</li>
								<li class="w-li">Какие стратегии дают стабильную доходность?</li>
								<li class="w-li">Какие стратегии используют наши клиенты?</li>
								<li class="w-li">Сколько денег потребуется на&nbsp;инвестиции в&nbsp;землю?</li>
								<li class="w-li">Каковы окупаемость вложений и&nbsp;скорость оборота?</li>
								<li class="w-li">Как именно вам лучше стартовать на&nbsp;рынке земли?</li>
							</ul>
						</div><!--.w-ways__container-t-->
						<div class="w-ways__container-b">
							<p>
								<a href="#" class="btn button_green button_green_sm">Получить материалы</a>
							</p>
						</div><!--.w-ways__container-b-->
					</div><!--.w-ways__container-->
				</div><!--col-->
			</div><!--.row-->
		</div><!--.container-fluid-->
	</div><!--.w-ways-->
</section><!--.w-ways__wrapper-->