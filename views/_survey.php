<link rel="stylesheet" href="app/css/w/survey/2.0.0.css">
<section class="w-survey__wrapper">
  <div class="w-survey">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <a href="#" class="w-survey__link link">
            <span class="w-survey__link-text"><b>Реальные</b> съемки местности</span>
            <span class="button_green w-survey__link-btn"><i class="arrow arrow_to-left"></i>Нажмите здесь<i class="arrow arrow_to-right"></i></span>
          </a>
        </div><!--col-->
      </div><!--.row-->
    </div><!--.container-->
  </div><!--.w-survey-->
</section><!--.w-survey__wrapper-->