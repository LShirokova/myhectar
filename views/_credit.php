<link rel="stylesheet" href="app/css/w/credit/2.0.0.css">
<section class="w-credit page-section" style="background-image: url('web/img/credit/section-bg.jpg');">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="w-credit__header clearfix">
					<div class="w-credit__sup-title">
						Хорошие новости
					</div>
					<div class="w-credit__logo">
						<a href=""><img src="web/img/credit/bank-logo.jpg" alt=""></a>
					</div>
					<div class="w-credit__title">
						<h2 class="h2"><b>Получите полноценную<br>
						дачу сейчас</b>  — оплатите потом</h2>
					</div>

				</div>
			</div>

		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="w-credit__content">
					<div class="w-credit__content-text">
						<p>Компания «Большая земля» совместно с АО «Россельхозбанк» предлагают вам льготные условия кредитования. Вы получите очень доступную ставку на развитие своей усадьбы или агробизнеса. Банк точно одобрит кредит под залог участка (даже если у вас серая зарплата).
						</p>
						<p>Условия кредита под залог участка площадью 1 гектар в поселке<br>
						Синергия-Волга:
						</p>
						<ul class="list-custom">
							<li>Сумма кредита от 700 000 до 1 500 000 рублей</li>
							<li>Срок от 1 до 5 лет</li>
							<li>Ставка от 6,75% годовых</li>
						</ul>
					</div>
					<p class="md-center"><strong>Используйте эти средства на строительство дома или агробизнес!</strong></p>
					<div class="w-credit__buttons-container">
						<button class="btn button w-credit__btn">Хочу хорошие условия!</button>
					</div>
				</div>

			</div>
		</div>
	</div>

</section>