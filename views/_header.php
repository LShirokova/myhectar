<link rel="stylesheet" href="app/css/w/header/2.0.0.css">

<header class="w-header">
  <div class="container-fluid">
    <div class="row">
      <div class="col-xxl-1 d-flex align-items-center align-xxl-items-end col-3 order-0">
        <a href="/" class="w-header__logo link">
          <img class="img-fluid" src="web/img/header/header__logo.png" alt="Мой Гектар">
        </a>
      </div><!--col-->
      <div class="col-xxl-4 col-12 d-xxl-flex align-items-end order-10 order-xxl-1">
        <nav id="w-header__nav" class="w-header__nav collapse navbar-collapse d-xxl-block d-none">
          <a href="#" class="w-header__nav-a link">Преимущества</a>
          <a href="#" class="w-header__nav-a link">3D-Тур</a>
          <a href="#" class="w-header__nav-a link">Цены</a>
          <a href="#" class="w-header__nav-a link">Кредит</a>
        </nav><!--.w-header__nav-->
      </div><!--col-->
      <div class="col-xxl-5 d-xxl-flex align-items-end d-none order-2">
        <div class="w-header__info">
          <b>Поселок Синергия-Волга</b> расположен на&nbsp;пересечении двух рек&nbsp;&mdash; Волги и&nbsp;Бойни,
          недалеко от&nbsp;Москвы по&nbsp;Новорижскому шоссе. Великолепный выбор для строительства
          усадьбы и&nbsp;организации личного хозяйства.
        </div><!--.w-header__info-->
      </div><!--col-->
      <div class="col-xxl-2 flex-column d-flex justify-content-xxl-end justify-content-lg-start align-items-end col-9 order-4">
        <button class="navbar-toggler d-xxl-none align-items-end d-block" type="button" data-toggle="collapse"
                data-target="#w-header__nav" aria-controls="w-header__nav" aria-expanded="false"
                aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <a href="tel:+74955454323" class="w-header__tel link">
          <span class="highlighting">+7 (495)</span> 545-43-23
        </a>
      </div><!--col-->
    </div><!--.row-->
  </div><!--.container-fluid-->
</header><!--.w-header-->