<link rel="stylesheet" href="app/css/w/advantages/2.0.0.css">
<section class="w-advantages__wrapper">
  <div class="w-advantages">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xl-6 col-md-7">
          <div class="w-advantages__col">
            <h2 class="h2 text-center"><b>Преимущества</b> поселка</h2>
            <div class="w-advantages__container">
              <div class="w-advantages__text1">
                Комплекс участков «Си&shy;нергия-Волга» расположен на пересечении рек Волга и Бойня,
                недалеко от Москвы по Новорижско&shy;му шоссе
              </div>
              <div class="w-advantages-feature__wrapper">
                <div class="w-advantages-feature">
                  <img class="w-advantages-feature__col" src="web/img/advantages/advantages-feature_1.png"
                       alt="Река Волга">
                  <span class="w-advantages-feature__col">Река Волга</span>
                </div><!--.w-advantages-feature-->
                <div class="w-advantages-feature">
                  <img class="w-advantages-feature__col" src="web/img/advantages/advantages-feature_2.png" alt="Лес">
                  <span class="w-advantages-feature__col">Лес</span>
                </div><!--.w-advantages-feature-->
                <div class="w-advantages-feature">
                  <img class="w-advantages-feature__col" src="web/img/advantages/advantages-feature_3.png"
                       alt="Рыбалка">
                  <span class="w-advantages-feature__col">Рыбалка</span>
                </div><!--.w-advantages-feature-->
                <div class="w-advantages-feature">
                  <img class="w-advantages-feature__col" src="web/img/advantages/advantages-feature_4.png"
                       alt="Коммуникации">
                  <span class="w-advantages-feature__col">Коммуникации</span>
                </div><!--.w-advantages-feature-->
              </div><!--.w-advantages-feature__wrapper-->

              <div class="w-advantages-feature-2__wrapper">
                <div class="w-advantages-feature-2">
                  <div class="w-advantages-feature-2__img">
                    <img src="web/img/advantages/advantages-feature-2_1.png" alt="Хорошая транспортная доступность"
                         class="img-fluid">
                  </div><!--.w-advantages-feature-2__img-->
                  <span class="w-advantages-feature-2__span">
                Хорошая транспортная доступность (два часа по&nbsp;магистрали), подъезд по&nbsp;асфальтированной
                трассе в&nbsp;любое время года
              </span>
                </div><!--.w-advantages-feature-2-->

                <div class="w-advantages-feature-2">
                  <div class="w-advantages-feature-2__img">
                    <img src="web/img/advantages/advantages-feature-2_2.png" alt="Развитая инфраструктура"
                         class="img-fluid">
                  </div><!--.w-advantages-feature-2__img-->
                  <span class="w-advantages-feature-2__span">
                Развитая инфраструктура (админстративный центр&nbsp;г. Ржев находит&shy;ся в&nbsp;2&nbsp;км от&nbsp;поселка)
              </span>
                </div><!--.w-advantages-feature-2-->

                <div class="w-advantages-feature-2">
                  <div class="w-advantages-feature-2__img">
                    <img src="web/img/advantages/advantages-feature-2_3.png"
                         alt="Развитием поселка занимается управляющая компания" class="img-fluid">
                  </div><!--.w-advantages-feature-2__img-->
                  <span class="w-advantages-feature-2__span">Развитием поселка занимается управляющая компания</span>
                </div><!--.w-advantages-feature-2-->

                <div class="w-advantages-feature-2">
                  <div class="w-advantages-feature-2__img">
                    <img src="web/img/advantages/advantages-feature-2_4.png"
                         alt="Оформление документов за&nbsp;счет компании" class="img-fluid">
                  </div><!--.w-advantages-feature-2__img-->
                  <span class="w-advantages-feature-2__span">
                Оформление документов на&nbsp;землю&nbsp;&mdash; за&nbsp;счет компании (30&nbsp;дней)
              </span>
                </div><!--.w-advantages-feature-2-->
              </div><!--.w-advantages-feature-2__wrapper-->
            </div><!--.w-advantages__container-->
          </div><!--.w-advantages__col-->
        </div><!--col-->
        <div class="col-xl-6 col-md-5">
          <div class="w-advantages__col w-advantages__col_2">
            <h2 class="h2 text-center"><b>Мощная</b> поддержка проекта</h2>
            <div class="w-advantages__container">
              <div class="w-advantages__text2">
                <p class="w-advantages__p">
                  Управляющая компания &laquo;Мой гектар&raquo; оказывает вам полную поддержку в&nbsp;развитии
                  участка и&nbsp;строительстве. После покупки земли вы&nbsp;быстро интег-
                  рируетесь в&nbsp;активное сообщество единомышленников, которое общими силами решает вопросы
                  благоустройства территории.
                </p>
                <p class="w-advantages__p">
                  Коммуникация происходит через мессенджеры и&nbsp;специализированные он&shy;лайн-сервисы.
                  Вы&nbsp;получите
                  доступ к&nbsp;системе на&nbsp;базе &laquo;1С&nbsp;ТСЖ&raquo;, где ведет&shy;ся учет всех рабочих
                  процессов.
                </p>
              </div><!--.w-advantages__text2-->
              <div class="w-slider owl-carousel">
                <div class="w-slider-item" style="background-image: url(web/img/advantages/w-advantages__slider-item_1.jpg)"></div>
                <div class="w-slider-item" style="background-image: url(web/img/advantages/w-advantages__slider-item_2.jpg)"></div>
                <div class="w-slider-item" style="background-image: url(web/img/advantages/w-advantages__slider-item_3.jpg)"></div>
                <div class="w-slider-item" style="background-image: url(web/img/advantages/w-advantages__slider-item_1.jpg)"></div>
                <div class="w-slider-item" style="background-image: url(web/img/advantages/w-advantages__slider-item_2.jpg)"></div>
                <div class="w-slider-item" style="background-image: url(web/img/advantages/w-advantages__slider-item_3.jpg)"></div>
              </div><!--.w-slider-->
            </div><!--.w-advantages__container-->
          </div><!--.w-advantages__col-->
        </div><!--col-->
        <div class="col-12">
          <p class="w-advantages__h3">Стоимость участков повышается каждые 2&nbsp;недели!</p>
          <p class="w-advantages__btn"><a href="#" class="btn button btn-block">Забронировать по&nbsp;вкусной цене</a></p>
        </div><!--col-->
      </div><!--.row-->
    </div><!--.container-fluid-->
  </div><!--.w-advantages-->
</section><!--.w-advantages__wrapper-->