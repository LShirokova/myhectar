<link rel="stylesheet" href="app/css/w/top/2.0.0.css">

<section class="w-top">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2 class="w-top__h">Участки на&nbsp;Волге</h2>
        <p class="w-top__p">Участок 1&nbsp;гектар = 100&nbsp;000&nbsp;Р</p>

        <a class="w-top__action link" href="#">
          <span class="w-top__action-h">Акция</span>
          <span class="w-top__action-p">
            О&nbsp;поселке Синергия-Волга <span class="highlighting">за&nbsp;2&nbsp;минуты</span>
          </span>
        </a><!--.w-top__action-->
      </div><!--col-->
    </div><!--.row-->
  </div><!--.container-->

  <div class="w-top-timer__wrapper">
    <div class="w-top-timer">
      <div class="container-fluid">
        <div class="row">
          <div class="col-xl-7 col-md-6 order-5 order-md-1">
            <i class="w-top__arrow"></i>
            <div class="w-top-timer__btn">
              <a href="#" class="button btn btn-block">Получить по&nbsp;акции</a>
            </div><!--.w-top-timer__btn-->
          </div><!--col-->
          <div class="col-xl-5 col-md-6 order-1 order-md-5">
            <h3 class="w-top-timer__h">Цена вырастет <br>на&nbsp;50% через </h3>
            <div id="js-countdown" class="w-top-timer__countdown">
              <div class="w-top-timer__countdown-item">
                <div id="js-countdown__days" class="js-countdown__days w-top-timer__countdown-item-numb"></div>
                <div id="js-countdown__daysText" class="js-countdown__daysText w-top-timer__countdown-item-text"></div>
              </div><!--.w-top-timer__countdown-item-->
              <div class="w-top-timer__countdown-item">
                <div id="js-countdown__hours" class="js-countdown__hours w-top-timer__countdown-item-numb"></div>
                <div id="js-countdown__hoursText" class="js-countdown__hoursText w-top-timer__countdown-item-text"></div>
              </div><!--.w-top-timer__countdown-item-->
              <div class="w-top-timer__countdown-item">
                <div id="js-countdown__minutes" class="js-countdown__minutes w-top-timer__countdown-item-numb"></div>
                <div id="js-countdown__minutesText" class="js-countdown__minutesText w-top-timer__countdown-item-text"></div>
              </div><!--.w-top-timer__countdown-item-->
            </div><!--.w-top-timer__countdown-->
          </div><!--col-->
        </div><!--.row-->
      </div><!--.container-->
    </div><!--.w-top-timer-->
  </div><!--.w-top-timer__wrapper-->
</section><!--.w-top-->