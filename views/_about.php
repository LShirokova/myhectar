<link rel="stylesheet" href="app/css/w/about/2.0.0.css">
<section class="w-about__wrapper">
  <div class="w-about">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xxxl-6 col-12">
          <div class="w-about__container">
            <h2 class="h2"><b>О&nbsp;компании</b> &laquo;Большая Земля&raquo;</h2>
            <p class="w-about__p">
              &laquo;Большая земля&raquo;&nbsp;&mdash; крупнейший собственник земли в&nbsp;Тверской области.
              У&nbsp;нас более 120&nbsp;000 гектаров в&nbsp;прямой собственности и&nbsp;около 100&nbsp;000 гектаров
              в&nbsp;управлении. Мы&nbsp;любим проекты, которые приносят пользу клиентам и&nbsp;меняют мир
              к&nbsp;лучшему. Проект &laquo;Мой гектар&raquo; реализуется при поддержке Союза садоводов России,
              АО&nbsp;&laquo;Россельхозбанк&raquo; и&nbsp;правительства Тверской области.
            </p>
            <p class="w-about__p">
              Участниками &laquo;Моего гектара&raquo; в&nbsp;2017 году стали более 1&nbsp;000 жителей центральных
              регионов. Помимо данного проекта, мы&nbsp;реализовали десятки девелоперских проектов и&nbsp;получили
              сотни позитивных отзывов от&nbsp;клиентов. О&nbsp;совместной с&nbsp;Союзом садоводов инициативе
              положительно отозвался премьер-министр РФ&nbsp;Дмитрий Медведев. Больше информации:
              <a class="link link_2" href="http://bigland.ru/">www.bigland.ru</a>
            </p>
            <div class="w-slider owl-carousel">
              <div class="w-slider-item" style="background-image: url(web/img/about/w-about__slider-item_1.jpg)"></div>
              <div class="w-slider-item" style="background-image: url(web/img/about/w-about__slider-item_2.jpg)"></div>
              <div class="w-slider-item" style="background-image: url(web/img/about/w-about__slider-item_3.jpg)"></div>
              <div class="w-slider-item" style="background-image: url(web/img/about/w-about__slider-item_1.jpg)"></div>
              <div class="w-slider-item" style="background-image: url(web/img/about/w-about__slider-item_2.jpg)"></div>
              <div class="w-slider-item" style="background-image: url(web/img/about/w-about__slider-item_3.jpg)"></div>
            </div><!--.w-slider-->
          </div><!--.w-about__container-->
        </div><!--col-->
        <div class="col-xxxl-6 col-12">
          <div class="w-about__container-2">
            <h2 class="h2 text-center"><b>О&nbsp;нас</b> говорят</h2>

            <div class="w-about-slider__wrapper">
              <div class="w-about-slider owl-carousel">
                <a href="#" class="w-about-slider__item link"
                   style="background-image: url(web/img/about/w-about__slider-item_4.jpg)">
									<span class="w-about-slider__item-text">
                    <span class="w-about-slider__item-text-xl">Олег Валенчук</span>
                    <span class="w-about-slider__item-text-sm">Председатель Совета садоводов&nbsp;РФ</span>
									</span>
                </a><!--.w-about-slider__item-->

                <a href="#" class="w-about-slider__item link"
                   style="background-image: url(web/img/about/w-about__slider-item_4.jpg)">
									<span class="w-about-slider__item-text">
                    <span class="w-about-slider__item-text-xl">Олег Валенчук</span>
                    <span class="w-about-slider__item-text-sm">Председатель Совета садоводов&nbsp;РФ</span>
									</span>
                </a><!--.w-about-slider__item-->

                <a href="#" class="w-about-slider__item link"
                   style="background-image: url(web/img/about/w-about__slider-item_4.jpg)">
									<span class="w-about-slider__item-text">
                    <span class="w-about-slider__item-text-xl">Олег Валенчук</span>
                    <span class="w-about-slider__item-text-sm">Председатель Совета садоводов&nbsp;РФ</span>
									</span>
                </a><!--.w-about-slider__item-->
              </div><!--.w-about-slider-->
              <div class="w-about-slider__dots"></div>
            </div><!--.w-about-slider__wrapper-->
          </div><!--.w-about__container-2-->
        </div><!--col-->
      </div><!--.row-->
    </div><!--.container-fluid-->
  </div><!--.w-about-->
</section><!--.w-about__wrapper-->