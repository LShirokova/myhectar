<link rel="stylesheet" href="app/css/w/booking/2.0.0.css">
<section class="w-booking page-section" style="background-image: url('web/img/booking/section-bg.jpg');">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="w-booking__header">
					<h2 class="h2 w-booking__title"><b>Забронируйте участок</b> Сейчас</h2>
				</div>
			</div>
		</div>
		<div class="row w-booking__container">
			<div class="col-md-12 list-sector owl-carousel">
				<div class="list-sector__item">
					<div class="list-sector__header" style="background-image: url('web/img/booking/list-sectoritem1.jpg');">
					</div>
					<div class="list-sector__content">
						<div class="list-sector__title">Зона на&nbsp;просторе</div>
						<div class="list-sector__description">
							100 000 рублей за участок
						</div>
						<div class="list-sector__buttons">
							<button class="btn button button_sm list-sector__button">
								Забронировать
							</button>
						</div>
					</div>
				</div>
				<div class="list-sector__item">
					<div class="list-sector__header" style="background-image: url('web/img/booking/list-sectoritem2.jpg');">
					</div>
					<div class="list-sector__content">
						<div class="list-sector__title">Зона у леса</div>
						<div class="list-sector__description">
							200 000 рублей за участок
						</div>
						<div class="list-sector__buttons">
							<button class="btn button button_sm list-sector__button">
								Забронировать
							</button>
						</div>
					</div>
				</div>
				<div class="list-sector__item">
					<div class="list-sector__header" style="background-image: url('web/img/booking/list-sectoritem3.jpg');">
					</div>
					<div class="list-sector__content">
						<div class="list-sector__title">Зона у реки</div>
						<div class="list-sector__description">
							300 000 рублей за участок
						</div>
						<div class="list-sector__buttons">
							<button class="btn button button_sm list-sector__button">
								Забронировать
							</button>
						</div>
					</div>
				</div>
			</div>


		</div>
	</div>

</section>