<link rel="stylesheet" href="app/css/w/land-selection/2.0.0.css">
<div class="w-land-selection__wrapper">
  <div class="w-land-selection">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xxl-3 col-md-5">
          <div class="w-land-selection__filter">
            <div class="w-land-selection__filter-content">
              <div class="d-xxl-none d-block text-center pb-2">
                <h2 class="h2"><b>Выберите</b> участок</h2>
                <a href="#" class="link link_2 text-uppercase">Кликните для подробностей</a>
              </div>

              <h3 class="h3 text-center d-block d-md-none">
                <a class="link" data-toggle="collapse" href="#collapseFilter" aria-expanded="true" aria-controls="collapseFilter">
                  Фильтр<i class="arrow arrow_down"></i>
                </a>
              </h3>
              <h3 class="h3 text-center d-none d-md-block">Фильтр</h3>
              <div class="w-land-selection__filter-collapse" id="collapseFilter">
                <div class="w-land-selection__filter-line">
                  <div class="w-land-selection__checkbox">
                    <input type="checkbox" class="w-land-selection__checkbox-input" id="w-land-selection__checkbox1">
                    <label class="w-land-selection__checkbox-label" for="w-land-selection__checkbox1">
                      Зона на&nbsp;просторе
                      <span class="w-land-selection__filter-highlighting">100&nbsp;000 рублей</span>
                    </label>
                  </div><!--.w-land-selection__checkbox-->
                </div><!--.w-land-selection__filter-line-->

                <div class="w-land-selection__filter-line">
                  <div class="w-land-selection__checkbox">
                    <input type="checkbox" class="w-land-selection__checkbox-input" id="w-land-selection__checkbox2">
                    <label class="w-land-selection__checkbox-label" for="w-land-selection__checkbox2">
                      Зона у&nbsp;леса
                      <span class="w-land-selection__filter-highlighting">200&nbsp;000 рублей</span>
                    </label>
                  </div><!--.w-land-selection__checkbox-->
                </div><!--.w-land-selection__filter-line-->

                <div class="w-land-selection__filter-line">
                  <div class="w-land-selection__checkbox">
                    <input type="checkbox" class="w-land-selection__checkbox-input" id="w-land-selection__checkbox3">
                    <label class="w-land-selection__checkbox-label" for="w-land-selection__checkbox3">
                      Зона у&nbsp;реки
                      <span class="w-land-selection__filter-highlighting">300&nbsp;000 рублей</span>
                    </label>
                  </div><!--.w-land-selection__checkbox-->
                </div><!--.w-land-selection__filter-line-->

                <div class="w-land-selection__filter-line w-land-selection__filter-line_next-section">
                  <div class="w-land-selection__checkbox">
                    <div class="w-land-selection__checkbox-label w-land-selection__checkbox-label_1 pt-2">Доступно</div>
                  </div><!--.w-land-selection__checkbox-->
                </div><!--.w-land-selection__filter-line-->

                <div class="w-land-selection__filter-line">
                  <div class="w-land-selection__checkbox">
                    <div class="w-land-selection__checkbox-label w-land-selection__checkbox-label_2 pt-2">Забронировано</div>
                  </div><!--.w-land-selection__checkbox-->
                </div><!--.w-land-selection__filter-line-->

                <div class="w-land-selection__filter-line">
                  <div class="w-land-selection__checkbox">
                    <div class="w-land-selection__checkbox-label w-land-selection__checkbox-label_3 pt-2">Продано</div>
                  </div><!--.w-land-selection__checkbox-->
                </div><!--.w-land-selection__filter-line-->
              </div><!--#collapseFilter-->
            </div><!--.w-land-selection__filter-content-->

            <a href="#" class="w-land-selection__filter-btn button_green btn d-none d-md-block">
              Помогите выбрать<i class="arrow arrow_to-right"></i>
            </a>
          </div><!--.w-land-selection__filter-->
        </div><!--col-->
        <div class="col-xxl-6 d-none d-xxl-flex">
          <div class="w-land-selection__content">
            <i class="w-land-selection__arrow"></i>
          </div><!--.w-land-selection__content-->
        </div><!--col-->
        <div class="col-xxl-3 col-md-6 d-flex justify-content-end">
          <div class="w-land-selection__btn">
            <a href="#" class="btn button btn-block">Забронировать</a>
          </div><!--.w-land-selection__btn-->
        </div><!--col-->
        <div class="col-12 d-block d-md-none">
          <a href="#" class="w-land-selection__filter-btn w-land-selection__filter-btn_2 button_green btn">
            Помогите выбрать<i class="arrow arrow_to-right"></i>
          </a>
        </div><!--col-->
      </div><!--.row-->
    </div><!--.container-fluid-->
  </div><!--.w-land-selection-->
</div><!--.w-land-selection__wrapper-->