<link rel="stylesheet" href="app/css/w/purchase-step/2.0.0.css">
<section class="w-purchase-step page-section" style="background-image: url('web/img/purchase-step/section-bg.jpg');">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<div class="w-purchase-step__header">
					<h2 class="w-purchase-step__title"><b>Как оформить участок</b> за&nbsp;три шага</h2>
				</div>
				<div class="w-purchase-step__container">
					<div class="w-purchase-step__item">
						<div class="w-purchase-step__img">
							<img src="web/img/purchase-step/step1.png" alt="">
						</div>
						<div class="w-purchase-step__name">
							Бронируете<br>
							понравившийся<br>
							участок
						</div>
					</div>

					<div class="w-purchase-step__item w-purchase-step__item_arrow">
						<div class="w-purchase-step__img">
							<img src="web/img/purchase-step/step-arrow.png" alt="">
						</div>
					</div>

					<div class="w-purchase-step__item">
						<div class="w-purchase-step__img">
							<img src="web/img/purchase-step/step2.png" alt="">
						</div>
						<div class="w-purchase-step__name">
							Оплачиваете<br>
							договор<br>
							купли/продажи
						</div>
					</div>

					<div class="w-purchase-step__item w-purchase-step__item_arrow w-purchase-step__item_mirror">
						<div class="w-purchase-step__img">
							<img src="web/img/purchase-step/step-arrow.png" alt="">
						</div>
					</div>

					<div class="w-purchase-step__item">
						<div class="w-purchase-step__img">
							<img src="web/img/purchase-step/step3.png" alt="">
						</div>
						<div class="w-purchase-step__name">
							Забираете<br>
							готовые документы<br>
							через 30 дней
						</div>
					</div>
				</div>
			</div>

		</div>

	</div>

</section>