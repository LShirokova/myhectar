<link rel="stylesheet" href="app/css/w/footer/2.0.0.css">
<section class="w-footer page-section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6 w-footer__column w-footer__column_left">
				<div class="w-footer__text">
					<p>Организатором акции является ООО «Большая земля»<br>
					При поддержке госпрограммы Министерства сельского хозяйства РФ
					Подробнее о компании «Большая земля»: <a href="http://www.bigland.ru">www.bigland.ru</a></p>
					<p><a href="">Политика конфиденциальности</a></p>

				</div>


			</div>
			<div class="col-lg-4 w-footer__column">
				<div class="social-list">
					<ul>
						<li class="social-list__item social-list__item"><a href="" class="social-list__link social-list__link_twitter"></a></li>
						<li class="social-list__item social-list__item"><a href="" class="social-list__link social-list__link_vkontakte"></a></li>
						<li class="social-list__item social-list__item"><a href="" class="social-list__link social-list__link_instagram"></a></li>
						<li class="social-list__item social-list__item"><a href="" class="social-list__link social-list__link_facebook"></a></li>
					</ul>
				</div>
			</div>
			<div class="col-lg-2 w-footer__column w-footer__column_right">
				<div class="w-footer__text">
					<p>ООО «Агросоюз»<br>
					ИНН 7702404582 <br>
					КПП 770201001 <br>
					ОГРН 1167746743581</p>
					<p><a href="">Онлайн оплата</a></p>
				</div>


			</div>
		</div>
	</div>

</section>