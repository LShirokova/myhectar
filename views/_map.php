<link rel="stylesheet" href="app/css/w/map/2.0.0.css">
<section class="w-map page-section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="w-map__content">
					<h2 class="h2 w-map__title">
						<b>Как добраться</b> до&nbsp;участка на&nbsp;Волге
					</h2>
					<div class="row">
						<div class="offset-md-1 col-md-5  w-map__column">
							<ul class="list-custom">
								<li>Новорижское шоссе всегда разгруженное, не бывает пробок</li>
								<li>Покрытие в отличном состоянии</li>
								<li>Если вы ездите на дачу раз в неделю, дорога точно не вызовет напряжения</li>
							</ul>
						</div>
						<div class="col-md-6 w-map__column w-map__column_left" >
							<p>Комплекс участков расположен на Новорижском шоссе. К поселку проложен удобный круглогодичный подъезд по асфальтированной трассе. Поблизости – все необходимые коммуникации и инфраструктура.</p>
						</div>
					</div>

				</div>

			</div>

		</div>
		<div class="row">
			<div class="w-map__buttons">
					<button class="btn button w-map__btn">
						Получить подробную карту проезда
					</button>
				</div>
		</div>
	</div>
	<div id="map">

	</div>
</section>