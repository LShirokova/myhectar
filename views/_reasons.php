<link rel="stylesheet" href="app/css/w/reasons/2.0.0.css">
<section class="w-reasons__wrapper">
  <div class="w-reasons">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <h2 class="h2"><b>Почему большой участок</b>&nbsp;&mdash; это бесценно?</h2>
          <p class="w-reasons__h3">На&nbsp;участке 1&nbsp;гектар вы&nbsp;уместите все, что хотели</p>

          <div class="w-reasons-features">
            <div class="w-reasons-features__item">
              <div class="w-reasons-features__item-img">
                <img src="web/img/reasons/reasons-features__item-img_1.png" alt="Большой дом" class="img-fluid">
              </div>
              Большой<br>дом
            </div><!--.w-reasons-features__item-->

            <div class="w-reasons-features__item">
              <div class="w-reasons-features__item-img">
                <img src="web/img/reasons/reasons-features__item-img_2.png" alt="Баня" class="img-fluid">
              </div>
              Баня
            </div><!--.w-reasons-features__item-->

            <div class="w-reasons-features__item">
              <div class="w-reasons-features__item-img">
                <img src="web/img/reasons/reasons-features__item-img_3.png" alt="Фруктовый сад" class="img-fluid">
              </div>
              Фруктовый<br>сад
            </div><!--.w-reasons-features__item-->

            <div class="w-reasons-features__item">
              <div class="w-reasons-features__item-img">
                <img src="web/img/reasons/reasons-features__item-img_4.png" alt="Зона отдыха" class="img-fluid">
              </div>
              Зона<br>отдыха
            </div><!--.w-reasons-features__item-->

            <div class="w-reasons-features__item">
              <div class="w-reasons-features__item-img">
                <img src="web/img/reasons/reasons-features__item-img_5.png" alt="Пруд" class="img-fluid">
              </div>
              Пруд
            </div><!--.w-reasons-features__item-->

            <div class="w-reasons-features__item">
              <div class="w-reasons-features__item-img">
                <img src="web/img/reasons/reasons-features__item-img_6.png" alt="Огород" class="img-fluid">
              </div>
              Огород
            </div><!--.w-reasons-features__item-->

            <div class="w-reasons-features__item">
              <div class="w-reasons-features__item-img">
                <img src="web/img/reasons/reasons-features__item-img_7.png" alt="Цветник" class="img-fluid">
              </div>
              Цветник
            </div><!--.w-reasons-features__item-->
          </div><!--.w-reasons-features-->

          <p class="w-reasons__p text-left">
            Природа исцеляет. Полноценный отдых на&nbsp;природе &mdash;действенный способ улучшить здоровье.
            <br>Рядом Волга и&nbsp;лес. Грибы и&nbsp;ягоды&nbsp;&mdash; всегда в&nbsp;шаговой доступности.
          </p>
        </div><!--col-->
      </div><!--.row-->
    </div><!--.container-fluid-->
  </div><!--.w-reasons-->
  <div class="w-reasons__wrapper-2">
    <div class="w-reasons">
      <div class="container-fluid">
        <div class="row">
          <div class="col w-reasons__col-1">
            <h3 class="w-reasons__h3">
              Вы&nbsp;знали, что на&nbsp;строительстве дома можно сэкономить
              до&nbsp;30% без ущерба качеству? <b>Мы&nbsp;расскажем вам:</b>
            </h3>
            <div class="row">
              <div class="col-xxl-5 col-12 d-flex align-items-center flex-column">
                <ul class="w-ul text-left">
                  <li class="w-li">Как правильно подготовить смету</li>
                  <li class="w-li">Как грамотно выбрать материалы</li>
                  <li class="w-li">Как оценить профессионализм бригады</li>
                  <li class="w-li mb-0">Как извлечь выгоду из&nbsp;сезонности</li>
                </ul>
              </div><!--col-->
              <div class="col-xxl-7 col-12 d-flex align-items-center">
                <p class="w-reasons__p mb-0">
                  В&nbsp;качестве бонуса&nbsp;&mdash; сравнение технологий загород&shy;ного строительства
                  + обзор современных строймате&shy;риалов.
                </p>
              </div><!--col-->
            </div><!--.row-->
          </div><!--col-->
          <div class="col w-reasons__col-2">
            <h3 class="w-reasons__h3 w-reasons__h3_2"><b>Получите материалы</b> на&nbsp;почту</h3>

            <div class="w-reasons__form-group">
              <input type="text" class="form-control w-reasons__form-control" placeholder="ФИО">
            </div><!--.w-reasons__form-group-->
            <div class="row">
              <div class="col-xxl-6 col-12">
                <div class="w-reasons__form-group">
                  <input type="email" class="form-control w-reasons__form-control" placeholder="E-mail">
                </div><!--.w-reasons__form-group-->
              </div><!--col-->
              <div class="col-xxl-6 col-12">
                <div class="w-reasons__form-group">
                  <input type="tel" class="form-control w-reasons__form-control" placeholder="Телефон">
                </div><!--.w-reasons__form-group-->
              </div><!--col-->
            </div><!--.row-->
            <div class="row">
              <div class="col-xxl-6 col-12">
                <div class="w-reasons__checkbox">
                  <input type="checkbox" class="w-reasons__checkbox-input" id="w-reasons__checkbox1" checked disabled>
                  <label class="w-reasons__checkbox-label" for="w-reasons__checkbox1">
                    Согласен с&nbsp;<a class="link_2 link" href="#">политикой конфиденциальности</a>
                  </label>
                </div><!--.w-reasons__checkbox-->
              </div><!--col-->
              <div class="col-xxl-6 col-12 text-right">
                <button class="btn button_green button_green_sm">Получить материалы</button>
              </div><!--col-->
            </div><!--.row-->
          </div><!--col-->
        </div><!--.row-->
      </div><!--.container-fluid-->
    </div><!--.w-reasons-->
  </div><!--.w-reasons__wrapper-2-->
</section><!--.w-reasons__wrapper-->