window.onload = init;

function init() {
  // countdown .w-top
  initializeClock('.js-countdown__days', '.js-countdown__daysText', '.js-countdown__hours', '.js-countdown__hoursText', '.js-countdown__minutes', '.js-countdown__minutesText', 'March 25 2018 23:59:59 GMT+03:00');

  $('.w-slider').owlCarousel({
    loop: true,
    margin: 6,
    nav: true,
    dots: false,
    responsive: {
      0: {
        items: 1
      },
      1439: {
        items: 3
      }
    }
  });

  $('.w-about-slider').owlCarousel({
    loop: true,
    margin: 0,
    nav: false,
    dots: true,
    items: 1,
    dotsContainer: '.w-about-slider__dots',
  });


  if ($(window).width() > 992) {
    scrollElement = $('.scroll-pane').jScrollPane();
    scrollApi = scrollElement.data('jsp');
    $('.scroll-pane').jScrollPane().bind(
          'mousewheel',
          function(e)
          {
              e.preventDefault();
          }

      );
     $('.list-custom_accordion li').on('click',function () {
      $(this).stop().toggleClass('active').find('.list-custom__description').stop().slideToggle();
      setTimeout(function () {
        scrollApi.reinitialise();
      },500)

    });
  }else{
    $('.list-custom_accordion li').on('click',function () {
      $(this).stop().toggleClass('active').find('.list-custom__description').stop().slideToggle();
    });
    $('.list-sector').owlCarousel({
        loop:true,
        dots: false,
        margin:10,
        nav:true,
        items:1
    });

  }


  ymaps.ready(init);
  var myMap;

  function init(){
      myMap = new ymaps.Map("map", {
          center: [44.437734, 34.096996],
          zoom: 16,
    controls: []
      }, {
          searchControlProvider: 'yandex#search'
      });
      MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
          '<div></div>'
      ),
      myPlacemarkWithContent = new ymaps.Placemark([44.435797, 34.096985], {
          hintContent: 'Собственный значок метки с контентом'
      }, {
          iconLayout: 'default#imageWithContent',
          iconImageHref: 'web/img/map/icon.png',
          iconImageOffset: [-58, -104],
          iconImageSize: [104, 116],
          iconContentLayout: MyIconContentLayout
      });

      myMap.geoObjects.add(myPlacemarkWithContent);
  }
}

// countdown
function getTimeRemaining(endtime) {
  var t = Date.parse(endtime) - Date.parse(new Date());
  var seconds = Math.floor((t / 1000) % 60);
  var minutes = Math.floor((t / 1000 / 60) % 60);
  var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
  var days = Math.floor(t / (1000 * 60 * 60 * 24));
  return {
    'total': t,
    'days': days,
    'hours': hours,
    'minutes': minutes,
    'seconds': seconds
  };
}

function initializeClock(daysEl, daysElText, hoursEl, hoursElText, minutesEl, minutesElText, endtime) {
  var timeinterval = setInterval(function () {
    var t = getTimeRemaining(endtime);
    $(daysEl).html( ('0' + t.days).slice(-2) );
    $(hoursEl).html( ('0' + t.hours).slice(-2) );
    $(minutesEl).html( ('0' + t.minutes).slice(-2) );
    $(daysElText).html( units(t.days, {nom: 'день', gen: 'дня', plu: 'дней'}) );
    $(hoursElText).html( units(t.hours, {nom: 'час', gen: 'часа', plu: 'часов'}) );
    $(minutesElText).html( units(t.minutes, {nom: 'минуту', gen: 'минуты', plu: 'минут'}) );
    if (t.total <= 0) {
      clearInterval(timeinterval);
    }
  }, 1000);
}

function units(num, cases) {
  num = Math.abs(num);
  var word = '';
  if (num.toString().indexOf('.') > -1) {
    word = cases.gen;
  } else {
    word = (
      num % 10 == 1 && num % 100 != 11
        ? cases.nom
        : num % 10 >= 2 && num % 10 <= 4 && (num % 100 < 10 || num % 100 >= 20)
        ? cases.gen
        : cases.plu
    );
  }
  return word;
}